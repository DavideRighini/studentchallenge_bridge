/*******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
* Copyright (C) 2015 Renesas Electronics Corporation. All rights reserved.
*******************************************************************************/
/*******************************************************************************
* File Name   : r_demo_api.c
*    @version
*        $Rev: 1732 $
*    @last editor
*        $Author: syama $
*    @date  
*        $Date:: 2016-09-26 07:41:35 +0200#$
* Description : 
******************************************************************************/

/******************************************************************************
Includes
******************************************************************************/
#include "r_typedefs.h"
#include "r_stdio_api.h"
#include "r_byte_swap.h"

/* g3 part */
#include "r_c3sap_api.h"

#ifdef R_SYNERGY_PLC
#include "tx_api.h"
#include "nx_api.h"
#include "main_thread.h"
#endif

/* app part */
#include "r_demo_api.h"
#include "r_demo_app.h"
#include "r_demo_app_thread.h"
#include "r_demo_status2text.h"

/******************************************************************************
Macro definitions
******************************************************************************/
/******************************************************************************
Typedef definitions
******************************************************************************/
/******************************************************************************
Exported global variables
******************************************************************************/
extern r_demo_config_t                  g_demo_config;
extern volatile r_demo_g3_cb_str_t      g_g3cb[R_G3_CH_MAX];

/******************************************************************************
Exported global variables (to be accessed by other files)
******************************************************************************/
/******************************************************************************
Private global variables and functions
******************************************************************************/


/******************************************************************************
Functions
******************************************************************************/


/*===========================================================================*/
/*    G3CTRL APIs                                                            */
/*===========================================================================*/


/******************************************************************************
* Function Name: R_DEMO_G3SetConfig
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_result_t R_DEMO_G3SetConfig(uint8_t chId,r_g3_set_config_req_t *config,
                              r_g3_set_config_cnf_t** cnf)
{
    r_result_t status;
    volatile r_g3_set_config_cnf_t *cfgCfm = &g_g3cb[chId].setConfig;
    *cnf = (r_g3_set_config_cnf_t *)cfgCfm;

#ifdef R_SYNERGY_PLC
    clear_flag((TX_EVENT_FLAGS_GROUP*)&g_g3cb[chId].statusFlags, R_FLAG_SET_CONFIG);
#else
   cfgCfm->status = R_DEMO_G3_STATUS_NOT_SET;  
#endif
    
    /* Check if verbose is enabled */
    if (g_demo_config.verboseEnabled == R_TRUE)
    {
        R_STDIO_Printf("\n -> Setting AdpConfig ");
    }

#if (defined R_SYNERGY_PLC)
    status = g_sf_plc_cpx0.p_api->g3_set_config_req (g_sf_plc_cpx0.p_ctrl, chId, config);
#else
    status = R_G3_SetConfigReq (chId, config);
#endif
    if (R_RESULT_SUCCESS != status)
    {
        if (g_demo_config.verboseEnabled == R_TRUE)
        {
            R_STDIO_Printf("failed!");
        }
        
        return R_RESULT_FAILED;
    }
    else
    {
#ifdef R_SYNERGY_PLC
        r_result_t result = R_RESULT_UNKNOWN;

        result = wait_for_flag((TX_EVENT_FLAGS_GROUP*)&g_g3cb[chId].statusFlags, R_FLAG_SET_CONFIG, TX_WAIT_FOREVER);

        if(result != R_RESULT_SUCCESS)
        {
            return result;
        }
#else
        /* Wait for completion of R_ADP_AdpmSet */
        while (cfgCfm->status == R_DEMO_G3_STATUS_NOT_SET){/**/}
#endif
       
        /* Check if verbose is enabled */
        if (g_demo_config.verboseEnabled == R_TRUE)
        {
            if (cfgCfm->status == R_G3_STATUS_SUCCESS)
            {
                R_STDIO_Printf("success. \n");
            }
            else
            {
                R_STDIO_Printf("failed. Status: %s(0x%.2X)\n", status_to_text(0, cfgCfm->status),cfgCfm->status);
            }
        }
        
        return R_RESULT_SUCCESS;
   }    
}
/******************************************************************************
   End of function  R_DEMO_G3SetConfig
******************************************************************************/



/******************************************************************************
* Function Name: R_DEMO_G3GetInfo
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_result_t R_DEMO_G3GetInfo(uint8_t chId,r_g3_get_info_req_t *req,
                              r_g3_get_info_cnf_t** cnf)
{
    r_result_t status;
    volatile r_g3_get_info_cnf_t *getInfoCfm = &g_g3cb[chId].getInfo;
    *cnf = (r_g3_get_info_cnf_t *)getInfoCfm;

#ifdef R_SYNERGY_PLC
    clear_flag((TX_EVENT_FLAGS_GROUP*)&g_g3cb[chId].statusFlags, R_FLAG_GET_INFO);
#else
    getInfoCfm->status = R_DEMO_G3_STATUS_NOT_SET;  
#endif
    
    /* Check if verbose is enabled */
    if (g_demo_config.verboseEnabled == R_TRUE)
    {
       R_STDIO_Printf("\n -> G3-GetInfo requesting... ");
    }

#if (defined R_SYNERGY_PLC)
    status = g_sf_plc_cpx0.p_api->g3_get_info_req (g_sf_plc_cpx0.p_ctrl, chId, req);
#else
    status = R_G3_GetInfoReq (chId, req);
#endif
    if (R_RESULT_SUCCESS != status)
    {
        if (g_demo_config.verboseEnabled == R_TRUE)
        {
            R_STDIO_Printf("failed!");
        }
        
        return R_RESULT_FAILED;
    }
    else
    {
#ifdef R_SYNERGY_PLC 
       r_result_t result = R_RESULT_UNKNOWN;

       result = wait_for_flag((TX_EVENT_FLAGS_GROUP*)&g_g3cb[chId].statusFlags, R_FLAG_GET_INFO, TX_WAIT_FOREVER); 

       if(result != R_RESULT_SUCCESS)
       {
           return result;
       }
#else
        /* Wait for completion of R_ADP_AdpmSet */
        while (getInfoCfm->status == R_DEMO_G3_STATUS_NOT_SET){/**/}
#endif
       
        /* Check if verbose is enabled */
        if (g_demo_config.verboseEnabled == R_TRUE)
        {
            if (getInfoCfm->status == R_G3_STATUS_SUCCESS)
            {
                R_STDIO_Printf("success. \n");
            }
            else
            {
                R_STDIO_Printf("failed. Status: %s(0x%.2X)\n", status_to_text(0, getInfoCfm->status),getInfoCfm->status);
            }
        }
        
        return R_RESULT_SUCCESS;
   }    
}
/******************************************************************************
   End of function  R_DEMO_G3GetInfo
******************************************************************************/



/******************************************************************************
* Function Name: R_DEMO_G3ClearInfo
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_result_t R_DEMO_G3ClearInfo(uint8_t chId, r_g3_clear_info_req_t *req,
                              r_g3_clear_info_cnf_t** cnf)
{
    r_result_t status;
    volatile r_g3_clear_info_cnf_t *clrInfoCfm = &g_g3cb[chId].clrInfo;
    *cnf = (r_g3_clear_info_cnf_t *)clrInfoCfm;

#ifdef R_SYNERGY_PLC
    clear_flag((TX_EVENT_FLAGS_GROUP*)&g_g3cb[chId].statusFlags, R_FLAG_CLEAR_INFO);
#else
    clrInfoCfm->status = R_DEMO_G3_STATUS_NOT_SET;  
#endif
    
    /* Check if verbose is enabled */
    if (g_demo_config.verboseEnabled == R_TRUE)
    {
       R_STDIO_Printf("\n -> G3-ClearInfo requesting... ");
    }

#if (defined R_SYNERGY_PLC)
    status = g_sf_plc_cpx0.p_api->g3_clear_info_req (g_sf_plc_cpx0.p_ctrl, chId, req);
#else
    status = R_G3_ClearInfoReq (chId, req);
#endif
    if (R_RESULT_SUCCESS != status)
    {
        if (g_demo_config.verboseEnabled == R_TRUE)
        {
            R_STDIO_Printf("failed!");
        }
        
        return R_RESULT_FAILED;
    }
    else
    {
#ifdef R_SYNERGY_PLC
       r_result_t result = R_RESULT_UNKNOWN;

       result = wait_for_flag((TX_EVENT_FLAGS_GROUP*)&g_g3cb[chId].statusFlags, R_FLAG_CLEAR_INFO, TX_WAIT_FOREVER);

       if(result != R_RESULT_SUCCESS)
       {
           return result;
       }
#else
        /* Wait for completion of R_ADP_AdpmSet */
        while (clrInfoCfm->status == R_DEMO_G3_STATUS_NOT_SET){/**/}
#endif
       
        /* Check if verbose is enabled */
        if (g_demo_config.verboseEnabled == R_TRUE)
        {
            if (clrInfoCfm->status == R_G3_STATUS_SUCCESS)
            {
                R_STDIO_Printf("success. \n");
            }
            else
            {
                R_STDIO_Printf("failed. Status: %s(0x%.2X)\n", status_to_text(0, clrInfoCfm->status),clrInfoCfm->status);
            }
        }
        
        return R_RESULT_SUCCESS;
   }    
}
/******************************************************************************
   End of function  R_DEMO_G3ClearInfo
******************************************************************************/


/******************************************************************************
* Function Name: R_DEMO_DeInit
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_result_t R_DEMO_DeInit(uint8_t chId)
{
    r_result_t status;

    R_STDIO_Printf("\n -> CPX3 DeInit (ch%d)...",chId);

#if (defined R_SYNERGY_PLC)
    status = g_sf_plc_cpx0.p_api->g3_deinit (g_sf_plc_cpx0.p_ctrl, chId, 2000);
#else
    status = R_G3_Deinit (chId, 2000);
#endif
    if (R_RESULT_SUCCESS != status)
    {
        return R_RESULT_FAILED;
    }
    else
    {
        R_STDIO_Printf("done.");
        return R_RESULT_SUCCESS;
    }
}
/******************************************************************************
   End of function  R_DEMO_DeInit
******************************************************************************/




/*===========================================================================*/
/*    MAC APIs                                                               */
/*===========================================================================*/

/******************************************************************************
* Function Name: R_DEMO_MacInit
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_result_t R_DEMO_MacInit(uint8_t chId)
{
    r_g3_init_req_t req;
    r_g3_callback_t callBack;
    r_result_t status;
 
    R_STDIO_Printf("\n -> CPX3 Init as MAC mode(ch%d)...",chId);
    
    req.g3mode = R_G3_MODE_MAC;
    req.init.mac.neighbourTableSize = R_DEMO_G3MAC_NEIGBOUR_TABLE_SIZE;
    req.init.mac.deviceTableSize = R_DEMO_G3MAC_DEVICE_TABLE_SIZE;
    req.init.mac.panDescriptorNum = R_DEMO_ADP_MAX_PAN_DESCRIPTORS;

    if(R_DEMO_InitMacCallBack(chId, &callBack) != R_RESULT_SUCCESS)
    {
        return R_RESULT_FAILED;
    }

#if (defined R_SYNERGY_PLC)
    status = g_sf_plc_cpx0.p_api->g3_init (g_sf_plc_cpx0.p_ctrl, chId, &callBack, &req, 2000);
#else
    status = R_G3_Init (chId, &callBack, &req, 2000);
#endif
    if (R_RESULT_SUCCESS != status)
    {
        return R_RESULT_FAILED;
    }
    else
    {
        R_STDIO_Printf ("done.");
        return R_RESULT_SUCCESS;
    }
}
/******************************************************************************
   End of function  R_DEMO_MacInit
******************************************************************************/



/******************************************************************************
* Function Name: R_DEMO_McpsData
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_result_t R_DEMO_McpsData(uint8_t chId, const r_g3mac_mcps_data_req_t* mcpsDataReq,
                              r_g3mac_mcps_data_cnf_t** cnf)
{
    r_result_t status;
    volatile r_g3mac_mcps_data_cnf_t *mcpsDataCfm = &g_g3cb[chId].mcpsDataCnf;
    *cnf = (r_g3mac_mcps_data_cnf_t *)mcpsDataCfm;

#ifdef R_SYNERGY_PLC
    clear_flag((TX_EVENT_FLAGS_GROUP*)&g_g3cb[chId].statusFlags, R_FLAG_MCPS_DATA);
#else
    mcpsDataCfm->status = R_DEMO_G3_STATUS_NOT_SET;  
#endif
    
    /* Check if verbose is enabled */
    if (g_demo_config.verboseEnabled == R_TRUE)
    {
       R_STDIO_Printf("\n -> Sending MAC Data frame...");
    }

#if (defined R_SYNERGY_PLC)
    status = g_sf_plc_cpx0.p_api->g3mac_mcps_data_req (g_sf_plc_cpx0.p_ctrl, chId, (r_g3mac_mcps_data_req_t *)mcpsDataReq);
#else
    status = R_G3MAC_McpsDataReq (chId, (r_g3mac_mcps_data_req_t *)mcpsDataReq);
#endif
    if (R_RESULT_SUCCESS != status)
    {
        if (g_demo_config.verboseEnabled == R_TRUE)
        {
            R_STDIO_Printf("failed!\n");
        }
        
        return R_RESULT_FAILED;
    }
    else
    {
#ifdef R_SYNERGY_PLC
        r_result_t result = R_RESULT_UNKNOWN;

        result = wait_for_flag((TX_EVENT_FLAGS_GROUP*)&g_g3cb[chId].statusFlags, R_FLAG_MCPS_DATA, TX_WAIT_FOREVER);

        if(result != R_RESULT_SUCCESS)
        {
            return result;
        }
#else
        /* Wait for completion of R_ADP_AdpmSet */
        while (mcpsDataCfm->status == R_DEMO_G3_STATUS_NOT_SET){/**/}
#endif
       
        /* Check if verbose is enabled */
        if (g_demo_config.verboseEnabled == R_TRUE)
        {
            if (mcpsDataCfm->status == R_G3MAC_STATUS_SUCCESS)
            {
                R_STDIO_Printf("success.\n");
            }
            else
            {
                R_STDIO_Printf("failed. Status: %s(0x%.2X)\n", status_to_text(R_G3_MODE_MAC, mcpsDataCfm->status),mcpsDataCfm->status);
            }
        }
        
        return R_RESULT_SUCCESS;
   }    
}
/******************************************************************************
   End of function  R_DEMO_McpsData
******************************************************************************/


/******************************************************************************
* Function Name: R_DEMO_MlmeReset
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_result_t R_DEMO_MlmeReset(uint8_t chId, const r_g3mac_mlme_reset_req_t* mlmeResetReq,
                              r_g3mac_mlme_reset_cnf_t** cnf)
{
    r_result_t status;
    volatile r_g3mac_mlme_reset_cnf_t *mlmeResetCfm = &g_g3cb[chId].mlmeResetCnf;
    *cnf = (r_g3mac_mlme_reset_cnf_t *)mlmeResetCfm;

#ifdef R_SYNERGY_PLC
    clear_flag((TX_EVENT_FLAGS_GROUP*)&g_g3cb[chId].statusFlags, R_FLAG_MLME_RESET);
#else
    mlmeResetCfm->status = R_DEMO_G3_STATUS_NOT_SET;  
#endif
    
    /* Check if verbose is enabled */
    if (g_demo_config.verboseEnabled == R_TRUE)
    {
       R_STDIO_Printf("\n -> Resetting MAC device...");
    }

#if (defined R_SYNERGY_PLC)
    status = g_sf_plc_cpx0.p_api->g3mac_mlme_reset_req (g_sf_plc_cpx0.p_ctrl, chId, (r_g3mac_mlme_reset_req_t *)mlmeResetReq);
#else
    status = R_G3MAC_MlmeResetReq (chId, (r_g3mac_mlme_reset_req_t *)mlmeResetReq);
#endif
    if (R_RESULT_SUCCESS != status)
    {
        if (g_demo_config.verboseEnabled == R_TRUE)
        {
            R_STDIO_Printf("failed!\n");
        }
        
        return R_RESULT_FAILED;
    }
    else
    {
#ifdef R_SYNERGY_PLC
        r_result_t result = R_RESULT_UNKNOWN;

        result = wait_for_flag((TX_EVENT_FLAGS_GROUP*)&g_g3cb[chId].statusFlags, R_FLAG_MLME_RESET, TX_WAIT_FOREVER);

        if(result != R_RESULT_SUCCESS)
        {
            return result;
        }
#else
        /* Wait for completion of R_ADP_AdpmReset */
        while (mlmeResetCfm->status == R_DEMO_G3_STATUS_NOT_SET){/**/}
#endif
       
        /* Check if verbose is enabled */
        if (g_demo_config.verboseEnabled == R_TRUE)
        {
            if (mlmeResetCfm->status == R_G3MAC_STATUS_SUCCESS)
            {
                R_STDIO_Printf("success.\n");
            }
            else
            {
                R_STDIO_Printf("failed. Status: %s(0x%.2X)\n", status_to_text(R_G3_MODE_MAC, mlmeResetCfm->status),mlmeResetCfm->status);
            }
        }
        
        return R_RESULT_SUCCESS;
   }    
}
/******************************************************************************
   End of function  R_DEMO_MlmeReset
******************************************************************************/


/******************************************************************************
* Function Name: R_DEMO_MlmeStart
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_result_t R_DEMO_MlmeStart(uint8_t chId, const r_g3mac_mlme_start_req_t* mlmeStartReq,
                              r_g3mac_mlme_start_cnf_t** cnf)
{
    r_result_t status;
    volatile r_g3mac_mlme_start_cnf_t *mlmeStartCfm = &g_g3cb[chId].mlmeStartCnf;
    *cnf = (r_g3mac_mlme_start_cnf_t *)mlmeStartCfm;
    
#ifdef R_SYNERGY_PLC
    clear_flag((TX_EVENT_FLAGS_GROUP*)&g_g3cb[chId].statusFlags, R_FLAG_MLME_START);
#else
    mlmeStartCfm->status = R_DEMO_G3_STATUS_NOT_SET;  
#endif
    
    /* Check if verbose is enabled */
    if (g_demo_config.verboseEnabled == R_TRUE)
    {
       R_STDIO_Printf("\n -> Startting MAC device...");
    }

#if (defined R_SYNERGY_PLC)
    status = g_sf_plc_cpx0.p_api->g3mac_mlme_start_req (g_sf_plc_cpx0.p_ctrl, chId, (r_g3mac_mlme_start_req_t *)mlmeStartReq);
#else
    status = R_G3MAC_MlmeStartReq (chId, (r_g3mac_mlme_start_req_t *)mlmeStartReq);
#endif
    if (R_RESULT_SUCCESS != status)
    {
        if (g_demo_config.verboseEnabled == R_TRUE)
        {
            R_STDIO_Printf("failed!\n");
        }
        
        return R_RESULT_FAILED;
    }
    else
    {
#ifdef R_SYNERGY_PLC
        r_result_t result = R_RESULT_UNKNOWN;

        result = wait_for_flag((TX_EVENT_FLAGS_GROUP*)&g_g3cb[chId].statusFlags, R_FLAG_MLME_START, TX_WAIT_FOREVER);

        if(result != R_RESULT_SUCCESS)
        {
            return result;
        }
#else
        /* Wait for completion of R_ADP_AdpmStart */
        while (mlmeStartCfm->status == R_DEMO_G3_STATUS_NOT_SET){/**/}
#endif
       
        /* Check if verbose is enabled */
        if (g_demo_config.verboseEnabled == R_TRUE)
        {
            if (mlmeStartCfm->status == R_G3MAC_STATUS_SUCCESS)
            {
                R_STDIO_Printf("success.\n");
            }
            else
            {
                R_STDIO_Printf("failed. Status: %s(0x%.2X)\n", status_to_text(R_G3_MODE_MAC, mlmeStartCfm->status),mlmeStartCfm->status);
            }
        }
        
        return R_RESULT_SUCCESS;
   }    
}
/******************************************************************************
   End of function  R_DEMO_MlmeStart
******************************************************************************/



/******************************************************************************
* Function Name: R_DEMO_MlmeScan
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_result_t R_DEMO_MlmeScan(uint8_t chId, const r_g3mac_mlme_scan_req_t* mlmeScanReq,
                              r_g3mac_mlme_scan_cnf_t** cnf)
{
    uint16_t i;
    r_result_t status;
    volatile r_g3mac_mlme_scan_cnf_t *mlmeScanCfm = &g_g3cb[chId].mlmeScanCnf;
    *cnf = (r_g3mac_mlme_scan_cnf_t *)mlmeScanCfm;

#ifdef R_SYNERGY_PLC
    clear_flag((TX_EVENT_FLAGS_GROUP*)&g_g3cb[chId].statusFlags, R_FLAG_MLME_SCAN);
#else
    mlmeScanCfm->status = R_DEMO_G3_STATUS_NOT_SET;  
#endif
    
    /* Check if verbose is enabled */
    if (g_demo_config.verboseEnabled == R_TRUE)
    {
       R_STDIO_Printf("\n -> Scanting MAC device...");
    }

#if (defined R_SYNERGY_PLC)
    status = g_sf_plc_cpx0.p_api->g3mac_mlme_scan_req (g_sf_plc_cpx0.p_ctrl, chId, (r_g3mac_mlme_scan_req_t *)mlmeScanReq);
#else
    status = R_G3MAC_MlmeScanReq (chId, (r_g3mac_mlme_scan_req_t *)mlmeScanReq);
#endif
    if (R_RESULT_SUCCESS != status)
    {
        if (g_demo_config.verboseEnabled == R_TRUE)
        {
            R_STDIO_Printf("failed!\n");
        }
        
        return R_RESULT_FAILED;
    }
    else
    {
#ifdef R_SYNERGY_PLC
        r_result_t result = R_RESULT_UNKNOWN;

        result = wait_for_flag((TX_EVENT_FLAGS_GROUP*)&g_g3cb[chId].statusFlags, R_FLAG_MLME_SCAN, TX_WAIT_FOREVER);

        if(result != R_RESULT_SUCCESS)
        {
            return result;
        }
#else
        /* Wait for completion of R_ADP_AdpmScan */
        while (mlmeScanCfm->status == R_DEMO_G3_STATUS_NOT_SET){/**/}
#endif
       
        /* Check if verbose is enabled */
        if (g_demo_config.verboseEnabled == R_TRUE)
        {
            if (mlmeScanCfm->status == R_G3MAC_STATUS_SUCCESS)
            {
                /* Show scan results */
                if (mlmeScanCfm->panCount != 0)
                {
                    R_STDIO_Printf("\n\n---------------Active network(s) found---------------------");

                    for (i = 0; i < mlmeScanCfm->panCount; i++)
                    {
                        R_STDIO_Printf("\n %d - PAN ID: 0x%.4X Short address: 0x%.4X RC Coordinator: 0x%.4X LQI: 0x%.2X", i, mlmeScanCfm->pPanList[i].panId,
                                                                                                                       R_BYTE_ArrToUInt16(mlmeScanCfm->pPanList[i].address),
                                                                                                                       mlmeScanCfm->pPanList[i].rcCoord,
                                                                                                                       mlmeScanCfm->pPanList[i].linkQuality);
                    }

                    R_STDIO_Printf("\n");
                }
                else
                {
                    R_STDIO_Printf("\n -> No active network(s) found.\n");
                }
            }
            else
            {
                R_STDIO_Printf("failed. Status: %s(0x%.2X)\n", status_to_text(R_G3_MODE_MAC, mlmeScanCfm->status),mlmeScanCfm->status);
            }
        }
        
        return R_RESULT_SUCCESS;
   }    
}
/******************************************************************************
   End of function  R_DEMO_MlmeScan
******************************************************************************/



/******************************************************************************
* Function Name: R_DEMO_MlmeSet
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_result_t R_DEMO_MlmeSet(uint8_t chId, const r_g3mac_mlme_set_req_t* mlmeSetReq,
                              r_g3mac_mlme_set_cnf_t** cnf)
{
    uint8_t len;
    r_result_t status;
    volatile r_g3mac_mlme_set_cnf_t *mlmeSetCfm = &g_g3cb[chId].mlmeSetCnf;
    *cnf = (r_g3mac_mlme_set_cnf_t *)mlmeSetCfm;

#ifdef R_SYNERGY_PLC
    clear_flag((TX_EVENT_FLAGS_GROUP*)&g_g3cb[chId].statusFlags, R_FLAG_MLME_SET);
#else
    mlmeSetCfm->status = R_DEMO_G3_STATUS_NOT_SET;  
#endif
    
    /* Check if verbose is enabled */
    if (g_demo_config.verboseEnabled == R_TRUE)
    {
       R_STDIO_Printf("\n -> Setting MAC PIB %s(0x%.4X) Index: %d...", ibid_to_text(R_G3_MODE_MAC, mlmeSetReq->pibAttributeId, &len), mlmeSetReq->pibAttributeId, mlmeSetReq->pibAttributeIndex);
    }

#if (defined R_SYNERGY_PLC)
    status = g_sf_plc_cpx0.p_api->g3mac_mlme_set_req (g_sf_plc_cpx0.p_ctrl, chId, (r_g3mac_mlme_set_req_t *)mlmeSetReq);
#else
    status = R_G3MAC_MlmeSetReq (chId, (r_g3mac_mlme_set_req_t *)mlmeSetReq);
#endif
    if (R_RESULT_SUCCESS != status)
    {
        if (g_demo_config.verboseEnabled == R_TRUE)
        {
            R_STDIO_Printf("failed!\n");
        }
        
        return R_RESULT_FAILED;
    }
    else
    {
#ifdef R_SYNERGY_PLC
        r_result_t result = R_RESULT_UNKNOWN;

        result = wait_for_flag((TX_EVENT_FLAGS_GROUP*)&g_g3cb[chId].statusFlags, R_FLAG_MLME_SET, TX_WAIT_FOREVER);

        if(result != R_RESULT_SUCCESS)
        {
            return result;
        }
#else
        /* Wait for completion of R_ADP_AdpmSet */
        while (mlmeSetCfm->status == R_DEMO_G3_STATUS_NOT_SET){/**/}
#endif
       
        /* Check if verbose is enabled */
        if (g_demo_config.verboseEnabled == R_TRUE)
        {
            if (mlmeSetCfm->status == R_G3MAC_STATUS_SUCCESS)
            {
                R_STDIO_Printf("success.\n");
            }
            else
            {
                R_STDIO_Printf("failed. Status: %s(0x%.2X)\n", status_to_text(R_G3_MODE_MAC, mlmeSetCfm->status),mlmeSetCfm->status);
            }
        }
        
        return R_RESULT_SUCCESS;
   }    
}
/******************************************************************************
   End of function  R_DEMO_MlmeSet
******************************************************************************/


/******************************************************************************
* Function Name: R_DEMO_MlmeGet
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_result_t R_DEMO_MlmeGet(uint8_t chId, const r_g3mac_mlme_get_req_t* mlmeGetReq,
                              r_g3mac_mlme_get_cnf_t** cnf)
{
    uint8_t len;
    r_result_t status;
    volatile r_g3mac_mlme_get_cnf_t *mlmeGetCfm = &g_g3cb[chId].mlmeGetCnf;
    *cnf = (r_g3mac_mlme_get_cnf_t *)mlmeGetCfm;

#ifdef R_SYNERGY_PLC
    clear_flag((TX_EVENT_FLAGS_GROUP*)&g_g3cb[chId].statusFlags, R_FLAG_MLME_GET);
#else
    mlmeGetCfm->status = R_DEMO_G3_STATUS_NOT_SET;
#endif

    /* Check if verbose is enabled */
    if (g_demo_config.verboseEnabled == R_TRUE)
    {
        R_STDIO_Printf("\n -> Getting MAC PIB %s(0x%.4X) Index: %d...", ibid_to_text(R_G3_MODE_MAC, mlmeGetReq->pibAttributeId, &len), mlmeGetReq->pibAttributeId, mlmeGetReq->pibAttributeIndex);
    }

#if (defined R_SYNERGY_PLC)
    status = g_sf_plc_cpx0.p_api->g3mac_mlme_get_req (g_sf_plc_cpx0.p_ctrl, chId, (r_g3mac_mlme_get_req_t *)mlmeGetReq);
#else
    status = R_G3MAC_MlmeGetReq (chId, (r_g3mac_mlme_get_req_t *)mlmeGetReq);
#endif
    if (R_RESULT_SUCCESS != status)
    {
         if (g_demo_config.verboseEnabled == R_TRUE)
        {
            R_STDIO_Printf("failed.\n");
        }
        
        return R_RESULT_FAILED;
    }
    else
    {
#ifdef R_SYNERGY_PLC
        r_result_t result = R_RESULT_UNKNOWN;

        result = wait_for_flag((TX_EVENT_FLAGS_GROUP*)&g_g3cb[chId].statusFlags, R_FLAG_MLME_GET, TX_WAIT_FOREVER);

        if(result != R_RESULT_SUCCESS)
        {
            return result;
        }
#else
        /* Wait for completion of R_ADP_AdpmGet */
        while (mlmeGetCfm->status == R_DEMO_G3_STATUS_NOT_SET){/**/}
#endif

        /* Check if verbose is enabled */
        if (g_demo_config.verboseEnabled == R_TRUE)
        {
            if (mlmeGetCfm->status == R_ADP_STATUS_SUCCESS)
            {
                R_STDIO_Printf("success.\n");
            }
            else
            {
                R_STDIO_Printf("failed. Status: %s(0x%.2X)\n", status_to_text(R_G3_MODE_MAC, mlmeGetCfm->status),mlmeGetCfm->status);
            }
        }

        return R_RESULT_SUCCESS;
    }  
}
/******************************************************************************
   End of function  R_DEMO_MlmeGet
******************************************************************************/




/******************************************************************************
* Function Name: R_DEMO_MlmeGetWrap
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_g3mac_status_t R_DEMO_MlmeGetWrap(uint8_t chId, uint16_t id, uint16_t index, uint8_t *val)
{
    r_g3mac_mlme_get_req_t    req;
    r_g3mac_mlme_get_cnf_t    *mlmeGetCfm;

    req.pibAttributeId = id;
    req.pibAttributeIndex = index;

    if(R_DEMO_MlmeGet(chId, &req, &mlmeGetCfm) == R_RESULT_SUCCESS)
    {
        R_memcpy(val, mlmeGetCfm->pibAttributeValue, sizeof(mlmeGetCfm->pibAttributeValue));
        return (r_g3mac_status_t)mlmeGetCfm->status;
    }
    else
    {
        return (r_g3mac_status_t)R_DEMO_G3_STATUS_FAILED;
    }
}
/******************************************************************************
   End of function  R_DEMO_MlmeGetWrap
******************************************************************************/



/******************************************************************************
* Function Name: R_DEMO_MlmeSetWrap
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_g3mac_status_t R_DEMO_MlmeSetWrap(uint8_t chId, uint16_t id, uint16_t index, uint8_t *val)
{
    r_g3mac_mlme_set_req_t    req;
    r_g3mac_mlme_set_cnf_t    *pCnf;

    req.pibAttributeId = id;
    req.pibAttributeIndex = index;
    req.pibAttributeValue = val;
    if(R_DEMO_MlmeSet(chId, &req, &pCnf) == R_RESULT_SUCCESS)
    {
        return (r_g3mac_status_t)pCnf->status;
    }
    else
    {
        return (r_g3mac_status_t)R_DEMO_G3_STATUS_FAILED;
    }
}
/******************************************************************************
   End of function  R_DEMO_MlmeSetWrap
******************************************************************************/




/*===========================================================================*/
/*    ADP APIs                                                               */
/*===========================================================================*/

/******************************************************************************
* Function Name: R_DEMO_AdpInit
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_result_t R_DEMO_AdpInit(uint8_t chId)
{
    r_g3_init_req_t req;
    r_g3_callback_t callBack;
    r_result_t status;

     
    R_STDIO_Printf("\n -> CPX3 Init as ADP mode(ch%d)...",chId);
    
    req.g3mode = R_G3_MODE_ADP;
    req.init.adp.adpdBuffNum = R_DEMO_ADP_ADPD_DATA_QUEUE_SIZE;
    req.init.adp.routeTableSize = R_DEMO_ADP_ROUTING_TABLE_SIZE;
    req.init.adp.neighbourTableSize = R_DEMO_G3MAC_NEIGBOUR_TABLE_SIZE;
    req.init.adp.deviceTableSize = R_DEMO_G3MAC_DEVICE_TABLE_SIZE;
    req.init.adp.panDescriptorNum = R_DEMO_ADP_MAX_PAN_DESCRIPTORS;
    req.init.adp.routeType = g_demo_config.routeType;

    if(R_DEMO_InitAdpCallBack(chId, &callBack) != R_RESULT_SUCCESS)
    {
        return R_RESULT_FAILED;
    }

#if (defined R_SYNERGY_PLC)
    status = g_sf_plc_cpx0.p_api->g3_init (g_sf_plc_cpx0.p_ctrl, chId, &callBack, &req, 2000);
#else
    status = R_G3_Init (chId, &callBack, &req, 2000);
#endif

    if (R_RESULT_SUCCESS != status)
    {
        return R_RESULT_FAILED;
    }
    else
    {
        R_STDIO_Printf("done.");
        return R_RESULT_SUCCESS;
    }
}
/******************************************************************************
   End of function  R_DEMO_AdpInit
******************************************************************************/


/******************************************************************************
* Function Name: R_DEMO_AdpmLbp
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_result_t R_DEMO_AdpmLbp(uint8_t chId, const r_adp_adpm_lbp_req_t* lbpReq,
                              r_adp_adpm_lbp_cnf_t** cnf)
{
    r_result_t status;
    volatile r_adp_adpm_lbp_cnf_t *lbpCfm = &g_g3cb[chId].adpmLbpCnf;
    *cnf = (r_adp_adpm_lbp_cnf_t *)lbpCfm;
    
#ifdef R_SYNERGY_PLC
    clear_flag((TX_EVENT_FLAGS_GROUP*)&g_g3cb[chId].statusFlags, R_FLAG_ADPM_LBP);
#else
    lbpCfm->status = R_DEMO_G3_STATUS_NOT_SET;
#endif

    /* Check if verbose is enabled */
    if (g_demo_config.verboseEnabled == R_TRUE)
    {
        R_STDIO_Printf("\n -> Sending LBP frame...");
    }
    
    /* Invoke ADPM-LBP.request */
#if (defined R_SYNERGY_PLC)
    status = g_sf_plc_cpx0.p_api->adp_m_lbp_req (g_sf_plc_cpx0.p_ctrl, chId,(r_adp_adpm_lbp_req_t *)lbpReq);
#else
    status = R_ADP_AdpmLbpReq (chId,(r_adp_adpm_lbp_req_t *)lbpReq);
#endif
    if (R_RESULT_SUCCESS != status)
    {
        /* Check if verbose is enabled */
        if (g_demo_config.verboseEnabled == R_TRUE)
        {
            R_STDIO_Printf("failed!");
        }
        
        return R_RESULT_FAILED;
    }
    else
    {
#ifdef R_SYNERGY_PLC
        r_result_t result = R_RESULT_UNKNOWN;

        result = wait_for_flag((TX_EVENT_FLAGS_GROUP*)&g_g3cb[chId].statusFlags, R_FLAG_ADPM_LBP, TX_WAIT_FOREVER);

        if(result != R_RESULT_SUCCESS)
        {
            return result;
        }
#else
        /* Wait for ADPM-LBP to finish. */  
        while (lbpCfm->status == R_DEMO_G3_STATUS_NOT_SET){/**/}
#endif

        /* Check if verbose is enabled */
        if (g_demo_config.verboseEnabled == R_TRUE)
        {
            if (lbpCfm->status == R_ADP_STATUS_SUCCESS)
            {
                R_STDIO_Printf("success. NSDU handle: 0x%.2X \n", lbpCfm->nsduHandle);
            }
            else
            {
                R_STDIO_Printf("failed. Status: %s(0x%.2X)\n", status_to_text(R_G3_MODE_ADP, lbpCfm->status),lbpCfm->status);
            }
        }
        
        return R_RESULT_SUCCESS;
    }
}
/******************************************************************************
   End of function  R_DEMO_AdpmLbp
******************************************************************************/



/******************************************************************************
* Function Name: R_DEMO_AdpmNetworkLeave
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_result_t R_DEMO_AdpmNetworkLeave(uint8_t chId, r_adp_adpm_network_leave_cnf_t** cnf)
{
    r_result_t status;
    volatile r_adp_adpm_network_leave_cnf_t *nwlCfm = &g_g3cb[chId].adpmNetworkLeaveCnf;
    *cnf = (r_adp_adpm_network_leave_cnf_t *)nwlCfm;

#ifdef R_SYNERGY_PLC
    clear_flag((TX_EVENT_FLAGS_GROUP*)&g_g3cb[chId].statusFlags, R_FLAG_ADPM_LEAVE);
#else
    nwlCfm->status = R_DEMO_G3_STATUS_NOT_SET;
#endif
    
    /* Check if verbose is enabled */
    if (g_demo_config.verboseEnabled == R_TRUE)
    {
        R_STDIO_Printf("\n -> Leaving network...");
    }

#if (defined R_SYNERGY_PLC)
    status = g_sf_plc_cpx0.p_api->adp_m_network_leave_req (g_sf_plc_cpx0.p_ctrl, chId);
#else
    status = R_ADP_AdpmNetworkLeaveReq (chId);
#endif
    if (R_RESULT_SUCCESS != status)
    {
       if (g_demo_config.verboseEnabled == R_TRUE)
       {
            R_STDIO_Printf("failed!");
       }
       
       return R_RESULT_FAILED;
    }
    else
    {
#ifdef R_SYNERGY_PLC
        r_result_t result = R_RESULT_UNKNOWN;

        result = wait_for_flag((TX_EVENT_FLAGS_GROUP*)&g_g3cb[chId].statusFlags, R_FLAG_ADPM_LEAVE, TX_WAIT_FOREVER);

        if(result != R_RESULT_SUCCESS)
        {
            return result;
        }
#else
        /* Wait for network leave to finish */
        while (nwlCfm->status == R_DEMO_G3_STATUS_NOT_SET){/**/}
#endif

        /* Check if verbose is enabled */
        if (g_demo_config.verboseEnabled == R_TRUE)
        {
            if (nwlCfm->status == R_ADP_STATUS_SUCCESS)
            {
                R_STDIO_Printf("success.\n");
            }
            else
            {
                R_STDIO_Printf("failed. Status: %s(0x%.2X)\n", status_to_text(R_G3_MODE_ADP, nwlCfm->status),nwlCfm->status);
            }
        }
        
        return R_RESULT_SUCCESS;
    }
}
/******************************************************************************
   End of function  R_DEMO_AdpmNetworkLeave
******************************************************************************/


/******************************************************************************
* Function Name: R_DEMO_AdpmNetworkJoin
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_result_t R_DEMO_AdpmNetworkJoin(uint8_t chId, const r_adp_adpm_network_join_req_t* nwjReq,
                                       r_adp_adpm_network_join_cnf_t** cnf)
{
    r_result_t status;
    volatile r_adp_adpm_network_join_cnf_t *nwjCfm = &g_g3cb[chId].adpmNetworkJoinCnf;
    *cnf = (r_adp_adpm_network_join_cnf_t *)nwjCfm;

#ifdef R_SYNERGY_PLC
    clear_flag((TX_EVENT_FLAGS_GROUP*)&g_g3cb[chId].statusFlags, R_FLAG_ADPM_JOIN);
#else
    nwjCfm->status = R_DEMO_G3_STATUS_NOT_SET;
#endif
    
    /* Check if verbose is enabled */
    if (g_demo_config.verboseEnabled == R_TRUE)
    {
        R_STDIO_Printf("\n -> Joining PAN with PanID:0x%.4X LBA:0x%02X%02X...", nwjReq->panId, nwjReq->lbaAddress[0],nwjReq->lbaAddress[1]);
    }

#if (defined R_SYNERGY_PLC)
    status = g_sf_plc_cpx0.p_api->adp_m_network_join_req (g_sf_plc_cpx0.p_ctrl, chId, (r_adp_adpm_network_join_req_t *)nwjReq);
#else
    status = R_ADP_AdpmNetworkJoinReq (chId, (r_adp_adpm_network_join_req_t *)nwjReq);
#endif
    if (R_RESULT_SUCCESS != status)
    {
       if (g_demo_config.verboseEnabled == R_TRUE)
       {
            R_STDIO_Printf("failed!");
       }
       
       return R_RESULT_FAILED;
    }
    else
    {
#ifdef R_SYNERGY_PLC
        r_result_t result = R_RESULT_UNKNOWN;

        result = wait_for_flag((TX_EVENT_FLAGS_GROUP*)&g_g3cb[chId].statusFlags, R_FLAG_ADPM_JOIN, TX_WAIT_FOREVER);

        if(result != R_RESULT_SUCCESS)
        {
            return result;
        }
#else
        /* Wait for network leave to finish */
        while (nwjCfm->status == R_DEMO_G3_STATUS_NOT_SET){/**/}
#endif

        /* Check if verbose is enabled */
        if (g_demo_config.verboseEnabled == R_TRUE)
        {
            if (nwjCfm->status == R_ADP_STATUS_SUCCESS)
            {
                R_STDIO_Printf("success.");
                R_STDIO_Printf(" Network Address: 0x%.4X\n", R_BYTE_ArrToUInt16((const uint8_t *)nwjCfm->networkAddress));
            }
            else
            {
                R_STDIO_Printf("failed. Status: %s(0x%.2X)\n", status_to_text(R_G3_MODE_ADP, nwjCfm->status),nwjCfm->status);
            }
        }
        
        return R_RESULT_SUCCESS;
    }
}
/******************************************************************************
   End of function  R_DEMO_AdpmNetworkJoin
******************************************************************************/


/******************************************************************************
* Function Name: R_DEMO_AdpmDiscovery
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_result_t R_DEMO_AdpmDiscovery(uint8_t chId, const r_adp_adpm_discovery_req_t* disReq,
                                     r_adp_adpm_discovery_cnf_t** cnf)
{
    uint16_t i;
    r_result_t status;
    volatile r_adp_adpm_discovery_cnf_t *disCfm = &g_g3cb[chId].adpmDiscoveryCnf;
    *cnf = (r_adp_adpm_discovery_cnf_t *)disCfm;

#ifdef R_SYNERGY_PLC
    clear_flag((TX_EVENT_FLAGS_GROUP*)&g_g3cb[chId].statusFlags, R_FLAG_ADPM_DISCOVERY);
#else
    disCfm->status = R_DEMO_G3_STATUS_NOT_SET;
#endif
    
    /* Check if verbose is enabled */
    if (g_demo_config.verboseEnabled == R_TRUE)
    {
        R_STDIO_Printf("\n -> Scanning for active networks...");
    }

    /* Start scanning */
#if (defined R_SYNERGY_PLC)
    status = g_sf_plc_cpx0.p_api->adp_m_discovery_req (g_sf_plc_cpx0.p_ctrl, chId, (r_adp_adpm_discovery_req_t *)disReq);
#else
    status = R_ADP_AdpmDiscoveryReq (chId, (r_adp_adpm_discovery_req_t *)disReq);
#endif
    if (R_RESULT_SUCCESS != status)
    {
        if (g_demo_config.verboseEnabled == R_TRUE)
        {
            R_STDIO_Printf("failed!");
        }
        
        return R_RESULT_FAILED;
    }
    else
    {
#ifdef R_SYNERGY_PLC
        r_result_t result = R_RESULT_UNKNOWN;

        result = wait_for_flag((TX_EVENT_FLAGS_GROUP*)&g_g3cb[chId].statusFlags, R_FLAG_ADPM_DISCOVERY, TX_WAIT_FOREVER);

        if(result != R_RESULT_SUCCESS)
        {
            return result;
        }
#else
        /* Wait for the scan to finish */
        while (disCfm->status == R_DEMO_G3_STATUS_NOT_SET){/**/}
#endif

        /* Check if verbose is enabled */
        if (g_demo_config.verboseEnabled == R_TRUE)
        {
            if (disCfm->status == R_ADP_STATUS_SUCCESS)
            {
                /* Show scan results */
                if (disCfm->PANCount != 0)
                {
                    R_STDIO_Printf("\n\n---------------Active network(s) found---------------------");

                    for (i = 0; i < disCfm->PANCount; i++)
                    {
                        R_STDIO_Printf("\n %d - PAN ID: 0x%.4X Short address: 0x%.4X RC Coordinator: 0x%.4X LQI: 0x%.2X", i, disCfm->PANDescriptor[i].panId,
                                                                                                                       R_BYTE_ArrToUInt16(disCfm->PANDescriptor[i].address),
                                                                                                                       disCfm->PANDescriptor[i].rcCoord,
                                                                                                                       disCfm->PANDescriptor[i].linkQuality);
                    }

                    R_STDIO_Printf("\n");
                }
                else
                {
                    R_STDIO_Printf("\n -> No active network(s) found.\n");
                }
            }
            else
            {
                R_STDIO_Printf("failed. Status: %s(0x%.2X)\n", status_to_text(R_G3_MODE_ADP, disCfm->status),disCfm->status);
            }
        }

        return R_RESULT_SUCCESS;
    }                                
}
/******************************************************************************
   End of function  R_DEMO_AdpmDiscovery
******************************************************************************/



/******************************************************************************
* Function Name: R_DEMO_AdpdData
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_result_t R_DEMO_AdpdData(uint8_t chId, const r_adp_adpd_data_req_t* dataReq,
                                r_adp_adpd_data_cnf_t** cnf)
{
    r_result_t status;
    volatile r_adp_adpd_data_cnf_t *dataCfm = &g_g3cb[chId].adpdDataCnf;
    *cnf = (r_adp_adpd_data_cnf_t *)dataCfm;

#ifdef R_SYNERGY_PLC
    clear_flag((TX_EVENT_FLAGS_GROUP*)&g_g3cb[chId].statusFlags, R_FLAG_ADPD_DATA);
#else
    dataCfm->status = R_DEMO_G3_STATUS_NOT_SET;
#endif

    /* Check if verbose is enabled */
    if (g_demo_config.verboseEnabled == R_TRUE)
    {
        R_STDIO_Printf("\n -> Sending data frame...");
    }
    
#ifndef R_SYNERGY_PLC
    R_DEMO_WaitcnfTimerOn();
#endif

    /* Invoke ADPD-DATA.request */
#if (defined R_SYNERGY_PLC)
    status = g_sf_plc_cpx0.p_api->adp_d_data_req (g_sf_plc_cpx0.p_ctrl, chId, (r_adp_adpd_data_req_t *)dataReq);
#else
    status = R_ADP_AdpdDataReq (chId, (r_adp_adpd_data_req_t *)dataReq);
#endif
    if (R_RESULT_SUCCESS != status)
    {
        if (g_demo_config.verboseEnabled == R_TRUE)
        {
            R_STDIO_Printf("failed!");
        }
#ifndef R_SYNERGY_PLC
        R_DEMO_WaitcnfTimerOff();
#endif
        return R_RESULT_FAILED;
    }
    else
    {
#ifdef R_SYNERGY_PLC
       r_result_t result = R_RESULT_UNKNOWN;

       result = wait_for_flag((TX_EVENT_FLAGS_GROUP*)&g_g3cb[chId].statusFlags, R_FLAG_ADPD_DATA, TX_WAIT_FOREVER);

       if(result != R_RESULT_SUCCESS)
       {
           return result;
       }
#else
        /* Wait for ADPD-DATA to finish */  
        while (
            (dataCfm->status == R_DEMO_G3_STATUS_NOT_SET)&&
            (R_DEMO_WaitcnfTimeout() == R_FALSE)){/**/};
#endif

        /* Check if verbose is enabled */
        if (g_demo_config.verboseEnabled == R_TRUE)
        {
            if (dataCfm->status == R_ADP_STATUS_SUCCESS)
            {
                R_STDIO_Printf("success. NSDU handle: 0x%.2X \n", dataCfm->nsduHandle);
            }
            else
            {
#ifdef R_SYNERGY_PLC
                R_STDIO_Printf("failed.Status: %s(0x%.2X)\n", status_to_text(R_G3_MODE_ADP, dataCfm->status),dataCfm->status);
#else
                if(R_DEMO_WaitcnfTimeout())
                {
                    R_STDIO_Printf("failed. by timeout \n");
                }
                else
                {
                    R_STDIO_Printf("failed.Status: %s(0x%.2X)\n", status_to_text(R_G3_MODE_ADP, dataCfm->status),dataCfm->status);
                }
#endif
            }
        }
#ifndef R_SYNERGY_PLC
        R_DEMO_WaitcnfTimerOff();
#endif
        return R_RESULT_SUCCESS;
    }
}
/******************************************************************************
   End of function  R_DEMO_AdpdData
******************************************************************************/


/******************************************************************************
* Function Name: R_DEMO_AdpmNetworkStart
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_result_t R_DEMO_AdpmNetworkStart(uint8_t chId, const r_adp_adpm_network_start_req_t* nwsReq,
                                        r_adp_adpm_network_start_cnf_t** cnf)
{
    r_result_t status;
    volatile r_adp_adpm_network_start_cnf_t *nwsCfm = &g_g3cb[chId].adpmNetworkStartCnf;
    *cnf = (r_adp_adpm_network_start_cnf_t *)nwsCfm;

#ifdef R_SYNERGY_PLC
    clear_flag((TX_EVENT_FLAGS_GROUP*)&g_g3cb[chId].statusFlags, R_FLAG_ADPM_START);
#else
    nwsCfm->status = R_DEMO_G3_STATUS_NOT_SET;
#endif
    
    /* Check if verbose is enabled */
    if (g_demo_config.verboseEnabled == R_TRUE)
    {
        R_STDIO_Printf("\n -> Starting PAN with ID: 0x%.4X...",  nwsReq->panId & 0xFCFF);
    }

#if (defined R_SYNERGY_PLC)
    status = g_sf_plc_cpx0.p_api->adp_m_network_start_req (g_sf_plc_cpx0.p_ctrl, chId, (r_adp_adpm_network_start_req_t *)nwsReq);
#else
    status = R_ADP_AdpmNetworkStartReq (chId, (r_adp_adpm_network_start_req_t *)nwsReq);
#endif
    if (R_RESULT_SUCCESS != status)
    {
        if (g_demo_config.verboseEnabled == R_TRUE)
        {
            R_STDIO_Printf("failed!");
        }
        
        return R_RESULT_FAILED;
    }
    else
    {
#ifdef R_SYNERGY_PLC
       r_result_t result = R_RESULT_UNKNOWN;

       result = wait_for_flag((TX_EVENT_FLAGS_GROUP*)&g_g3cb[chId].statusFlags, R_FLAG_ADPM_START, TX_WAIT_FOREVER);

       if(result != R_RESULT_SUCCESS)
       {
           return result;
       }
#else
        /* Wait for completion of R_ADP_AdpmNetworkStart */
        while (nwsCfm->status == R_DEMO_G3_STATUS_NOT_SET){/**/}
#endif

        /* Check if verbose is enabled */
        if (g_demo_config.verboseEnabled == R_TRUE)
        {
            if (nwsCfm->status == R_ADP_STATUS_SUCCESS)
            {
                R_STDIO_Printf("success.\n");
            }
            else
            {
                R_STDIO_Printf("failed. Status: %s(0x%.2X)\n", status_to_text(R_G3_MODE_ADP, nwsCfm->status),nwsCfm->status);
            }
        }

        return R_RESULT_SUCCESS;
    }
}
/******************************************************************************
   End of function  R_DEMO_AdpmNetworkStart
******************************************************************************/


/******************************************************************************
* Function Name: R_DEMO_AdpmSet
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_result_t R_DEMO_AdpmSet(uint8_t chId, const r_adp_adpm_set_req_t* setReq,
                              r_adp_adpm_set_cnf_t** cnf)
{
    r_result_t status;
    uint8_t len;
    volatile r_adp_adpm_set_cnf_t *setCfm = &g_g3cb[chId].adpmSetCnf;
    *cnf = (r_adp_adpm_set_cnf_t *)setCfm;

#ifdef R_SYNERGY_PLC
    clear_flag((TX_EVENT_FLAGS_GROUP*)&g_g3cb[chId].statusFlags, R_FLAG_ADPM_SET);
#else
   setCfm->status = R_DEMO_G3_STATUS_NOT_SET;  
#endif
    
    /* Check if verbose is enabled */
    if (g_demo_config.verboseEnabled == R_TRUE)
    {
       R_STDIO_Printf("\n -> Setting ADP IB %s(0x%.2X) Index: %d...", ibid_to_text(R_G3_MODE_ADP, setReq->aibAttributeId, &len), setReq->aibAttributeId, setReq->aibAttributeIndex);
    }

#if (defined R_SYNERGY_PLC)
    status = g_sf_plc_cpx0.p_api->adp_m_set_req (g_sf_plc_cpx0.p_ctrl, chId, (r_adp_adpm_set_req_t *)setReq);
#else
    status = R_ADP_AdpmSetReq (chId, (r_adp_adpm_set_req_t *)setReq);
#endif
    if (R_RESULT_SUCCESS != status)
    {
        if (g_demo_config.verboseEnabled == R_TRUE)
        {
            R_STDIO_Printf("failed!");
        }
        
        return R_RESULT_FAILED;
    }
    else
    {
#ifdef R_SYNERGY_PLC
       r_result_t result = R_RESULT_UNKNOWN;

       result = wait_for_flag((TX_EVENT_FLAGS_GROUP*)&g_g3cb[chId].statusFlags, R_FLAG_ADPM_SET, TX_WAIT_FOREVER);

       if(result != R_RESULT_SUCCESS)
       {
           return result;
       }
#else
        /* Wait for completion of R_ADP_AdpmSet */
        while (setCfm->status == R_DEMO_G3_STATUS_NOT_SET){/**/}
#endif
       
        /* Check if verbose is enabled */
        if (g_demo_config.verboseEnabled == R_TRUE)
        {
            if (setCfm->status == R_ADP_STATUS_SUCCESS)
            {
                R_STDIO_Printf("success.\n");
            }
            else
            {
                R_STDIO_Printf("failed. Status: %s(0x%.2X)\n", status_to_text(R_G3_MODE_ADP, setCfm->status),setCfm->status);
            }
        }
        
        return R_RESULT_SUCCESS;
   }    
}
/******************************************************************************
   End of function  R_DEMO_AdpmSet
******************************************************************************/



/******************************************************************************
* Function Name: R_DEMO_AdpmGet
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_result_t R_DEMO_AdpmGet(uint8_t chId, const r_adp_adpm_get_req_t* getReq,
                              r_adp_adpm_get_cnf_t** cnf)
{
    uint8_t len;
    r_result_t status;
    volatile r_adp_adpm_get_cnf_t *getCfm = &g_g3cb[chId].adpmGetCnf;
    *cnf = (r_adp_adpm_get_cnf_t *)getCfm;

#ifdef R_SYNERGY_PLC
    clear_flag((TX_EVENT_FLAGS_GROUP*)&g_g3cb[chId].statusFlags, R_FLAG_ADPM_GET);
#else
    getCfm->status = R_DEMO_G3_STATUS_NOT_SET;
#endif
        
    /* Check if verbose is enabled */
    if (g_demo_config.verboseEnabled == R_TRUE)
    {
        R_STDIO_Printf("\n -> Getting ADP IB %s(0x%.2X) Index: %d...", ibid_to_text(R_G3_MODE_ADP, getReq->aibAttributeId, &len), getReq->aibAttributeId, getReq->aibAttributeIndex);
    }

#if (defined R_SYNERGY_PLC)
    status = g_sf_plc_cpx0.p_api->adp_m_get_req (g_sf_plc_cpx0.p_ctrl, chId, (r_adp_adpm_get_req_t *)getReq);
#else
    status = R_ADP_AdpmGetReq (chId, (r_adp_adpm_get_req_t *)getReq);
#endif
    if (R_RESULT_SUCCESS != status)
    {
         if (g_demo_config.verboseEnabled == R_TRUE)
        {
            R_STDIO_Printf("failed");
        }
        
        return R_RESULT_FAILED;
    }
    else
    {
#ifdef R_SYNERGY_PLC
       r_result_t result = R_RESULT_UNKNOWN;

       result = wait_for_flag((TX_EVENT_FLAGS_GROUP*)&g_g3cb[chId].statusFlags, R_FLAG_ADPM_GET, TX_WAIT_FOREVER);

       if(result != R_RESULT_SUCCESS)
       {
           return result;
       }
#else
        /* Wait for completion of R_ADP_AdpmGet */
        while (getCfm->status == R_DEMO_G3_STATUS_NOT_SET){/**/}
#endif

        /* Check if verbose is enabled */
        if (g_demo_config.verboseEnabled == R_TRUE)
        {
            if (getCfm->status == R_ADP_STATUS_SUCCESS)
            {
                R_STDIO_Printf("success.\n");
            }
            else
            {
                R_STDIO_Printf("failed. Status: %s(0x%.2X)\n", status_to_text(R_G3_MODE_ADP, getCfm->status),getCfm->status);

            }
        }

        return R_RESULT_SUCCESS;
    }  
}
/******************************************************************************
   End of function  R_DEMO_AdpmGet
******************************************************************************/



/******************************************************************************
* Function Name: R_DEMO_AdpmRouteDiscovery
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_result_t R_DEMO_AdpmRouteDiscovery(uint8_t chId, const r_adp_adpm_route_disc_req_t* rdisReq,
                                          r_adp_adpm_route_disc_cnf_t** cnf)
{
    r_result_t status;
    volatile r_adp_adpm_route_disc_cnf_t *rdisCfm = &g_g3cb[chId].adpmRouteDiscoveryCnf;
    *cnf = (r_adp_adpm_route_disc_cnf_t *)rdisCfm;

#ifdef R_SYNERGY_PLC
    clear_flag((TX_EVENT_FLAGS_GROUP*)&g_g3cb[chId].statusFlags, R_FLAG_ADPM_ROUTE);
#else
    rdisCfm->status = R_DEMO_G3_STATUS_NOT_SET;
#endif

    /* Check if verbose is enabled */
    if (g_demo_config.verboseEnabled == R_TRUE)
    {
        R_STDIO_Printf("\n -> Starting route discovery...");
    }

#if (defined R_SYNERGY_PLC)
    status = g_sf_plc_cpx0.p_api->adp_m_route_discovery_req (g_sf_plc_cpx0.p_ctrl, chId, (r_adp_adpm_route_disc_req_t *)rdisReq);
#else
    status = R_ADP_AdpmRouteDiscoveryReq (chId, (r_adp_adpm_route_disc_req_t *)rdisReq);
#endif
    if (R_RESULT_SUCCESS != status)
    {
         if (g_demo_config.verboseEnabled == R_TRUE)
        {
            R_STDIO_Printf("failed!");
        }
        
        return R_RESULT_FAILED;
    }
    else
    {
#ifdef R_SYNERGY_PLC
       r_result_t result = R_RESULT_UNKNOWN;

       result = wait_for_flag((TX_EVENT_FLAGS_GROUP*)&g_g3cb[chId].statusFlags, R_FLAG_ADPM_ROUTE, TX_WAIT_FOREVER);

       if(result != R_RESULT_SUCCESS)
       {
           return result;
       }
#else
        /* Wait for completion of Route Discovery */
        while (rdisCfm->status == R_DEMO_G3_STATUS_NOT_SET){/**/}
#endif
        
        /* Check if verbose is enabled */
        if (g_demo_config.verboseEnabled == R_TRUE)
        {
            if (rdisCfm->status == R_ADP_STATUS_SUCCESS)
            {
                R_STDIO_Printf("success.\n");
            }
            else
            {
                R_STDIO_Printf("failed. Status: %s(0x%.2X)\n", status_to_text(R_G3_MODE_ADP, rdisCfm->status),rdisCfm->status);
            }
        }

        return R_RESULT_SUCCESS;
    }
}
/******************************************************************************
   End of function  R_DEMO_AdpmRouteDiscovery
******************************************************************************/


/******************************************************************************
* Function Name: R_DEMO_AdpmPathDiscovery
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_result_t R_DEMO_AdpmPathDiscovery(uint8_t chId, const r_adp_adpm_path_discovery_req_t* pdisReq,
                                         r_adp_adpm_path_discovery_cnf_t** cnf)
{
    uint8_t i;
    r_result_t status;
    volatile r_adp_adpm_path_discovery_cnf_t *pdisCfm = &g_g3cb[chId].adpmPathDiscoveryCnf;
    *cnf = (r_adp_adpm_path_discovery_cnf_t *)pdisCfm;

    pdisCfm->status = R_DEMO_G3_STATUS_NOT_SET;

#ifdef R_SYNERGY_PLC
    clear_flag((TX_EVENT_FLAGS_GROUP*)&g_g3cb[chId].statusFlags, R_FLAG_ADPM_PATH);
#else

#endif

    /* Check if verbose is enabled */
    if (g_demo_config.verboseEnabled == R_TRUE)
    {
        R_STDIO_Printf("\n -> Starting path discovery...");
    }

#if (defined R_SYNERGY_PLC)
    status = g_sf_plc_cpx0.p_api->adp_m_path_discovery_req (g_sf_plc_cpx0.p_ctrl, chId, (r_adp_adpm_path_discovery_req_t *)pdisReq);
#else
    status = R_ADP_AdpmPathDiscoveryReq (chId, (r_adp_adpm_path_discovery_req_t *)pdisReq);
#endif
    if (R_RESULT_SUCCESS != status)
    {
        if (g_demo_config.verboseEnabled == R_TRUE)
        {
            R_STDIO_Printf("failed!");
        }
        
        return R_RESULT_FAILED;
    }
    else
    {
#ifdef R_SYNERGY_PLC
       r_result_t result = R_RESULT_UNKNOWN;

       result = wait_for_flag((TX_EVENT_FLAGS_GROUP*)&g_g3cb[chId].statusFlags, R_FLAG_ADPM_PATH, TX_WAIT_FOREVER);

       if(result != R_RESULT_SUCCESS)
       {
           return result;
       }
#else
        /* Wait for completion of Path Discovery */
        while (pdisCfm->status == R_DEMO_G3_STATUS_NOT_SET){/**/}
#endif
        
        if (pdisCfm->status == R_ADP_STATUS_SUCCESS)
        {
            R_STDIO_Printf("success.");
        }
        else
        {
            R_STDIO_Printf("failed.");
        }

        R_STDIO_Printf("\n");
        
        R_STDIO_Printf("\nDestination address: 0x%.4X", R_BYTE_ArrToUInt16((const uint8_t *)pdisCfm->dstAddr));
        R_STDIO_Printf("\nMetric Type: 0x%.2X", pdisCfm->pathMetricType);
        R_STDIO_Printf("\nPath Table Entries: 0x%.2X", pdisCfm->pathTableEntries);
        R_STDIO_Printf("\nStatus: %s(0x%.2X)", status_to_text(R_G3_MODE_ADP, pdisCfm->status),pdisCfm->status);

        R_STDIO_Printf("\n\n---------------Route hops---------------------");

        for (i = 0; i < pdisCfm->pathTableEntries; i++)
        {
            R_STDIO_Printf("\n %d - Path address: 0x%.4X", i, R_BYTE_ArrToUInt16(pdisCfm->pathTable[i].pathAddress));
            R_STDIO_Printf("\n %d - Link cost: 0x%.2X", i, pdisCfm->pathTable[i].linkCost);
            R_STDIO_Printf("\n %d - Metric not supported: 0x%.2X", i, pdisCfm->pathTable[i].mns);
        }

        R_STDIO_Printf("\n");

        return R_RESULT_SUCCESS;
    }   
}
/******************************************************************************
   End of function  R_DEMO_AdpmPathDiscovery
******************************************************************************/


/******************************************************************************
* Function Name: R_DEMO_AdpmReset
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_result_t R_DEMO_AdpmReset(uint8_t chId, r_adp_adpm_reset_cnf_t** cnf)
{
    r_result_t status;
    volatile r_adp_adpm_reset_cnf_t *rstCfm = &g_g3cb[chId].adpmResetCnf;
    *cnf = (r_adp_adpm_reset_cnf_t *)rstCfm;

#ifdef R_SYNERGY_PLC
    clear_flag((TX_EVENT_FLAGS_GROUP*)&g_g3cb[chId].statusFlags, R_FLAG_ADPM_RESET);
#else
    rstCfm->status = R_DEMO_G3_STATUS_NOT_SET;
#endif
    
    /* Check if verbose is enabled */
    if (g_demo_config.verboseEnabled == R_TRUE)
    {
        R_STDIO_Printf("\n -> Resetting device...");
    }

#if (defined R_SYNERGY_PLC)
    status = g_sf_plc_cpx0.p_api->adp_m_reset_req (g_sf_plc_cpx0.p_ctrl, chId);
#else
    status = R_ADP_AdpmResetReq (chId);
#endif
    if (R_RESULT_SUCCESS != status)
    {
        if (g_demo_config.verboseEnabled == R_TRUE)
        {
            R_STDIO_Printf("failed!");
        }
        
        return R_RESULT_FAILED;
    }
    else
    {
#ifdef R_SYNERGY_PLC
       r_result_t result = R_RESULT_UNKNOWN;

       result = wait_for_flag((TX_EVENT_FLAGS_GROUP*)&g_g3cb[chId].statusFlags, R_FLAG_ADPM_RESET, TX_WAIT_FOREVER);

       if(result != R_RESULT_SUCCESS)
       {
           return result;
       }
#else
        /* Wait for completion of R_ADP_AdpmNetworkStart */
        while (rstCfm->status == R_DEMO_G3_STATUS_NOT_SET){/**/}
#endif

        /* Check if verbose is enabled */
        if (g_demo_config.verboseEnabled == R_TRUE)
        {
            if (rstCfm->status == R_ADP_STATUS_SUCCESS)
            {
                R_STDIO_Printf("success.\n");
            }
            else
            {
                R_STDIO_Printf("failed. Status: %s(0x%.2X)\n", status_to_text(R_G3_MODE_ADP, rstCfm->status),rstCfm->status);
            }
        }

        return R_RESULT_SUCCESS;
    }
}
/******************************************************************************
   End of function  R_DEMO_AdpmReset
******************************************************************************/


/******************************************************************************
* Function Name: R_DEMO_AdpmGetWrap
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_adp_status_t R_DEMO_AdpmGetWrap(uint8_t chId, r_adp_ib_id_t id, uint16_t index, uint8_t *val)
{
    r_adp_adpm_get_req_t    req;
    r_adp_adpm_get_cnf_t    *getCfm;

    req.aibAttributeId = id;
    req.aibAttributeIndex = index;

    if(R_DEMO_AdpmGet(chId, &req, &getCfm) == R_RESULT_SUCCESS)
    {
        R_memcpy(val, getCfm->aibAttributeValue, sizeof(getCfm->aibAttributeValue));
        return (r_adp_status_t)getCfm->status;
    }
    else
    {
        return (r_adp_status_t)R_DEMO_G3_STATUS_FAILED;
    }
}
/******************************************************************************
   End of function  R_DEMO_AdpmGetWrap
******************************************************************************/



/******************************************************************************
* Function Name: R_DEMO_AdpmSetWrap
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_adp_status_t R_DEMO_AdpmSetWrap(uint8_t chId, r_adp_ib_id_t id, uint16_t index, uint8_t *val)
{
    r_adp_adpm_set_req_t    req;
    r_adp_adpm_set_cnf_t    *pCnf;

    req.aibAttributeId = id;
    req.aibAttributeIndex = index;
    req.aibAttributeValue = val;
    if(R_DEMO_AdpmSet(chId, &req, &pCnf) == R_RESULT_SUCCESS)
    {
        return (r_adp_status_t)pCnf->status;
    }
    else
    {
        return (r_adp_status_t)R_DEMO_G3_STATUS_FAILED;
    }
}
/******************************************************************************
   End of function  R_DEMO_AdpmSetWrap
******************************************************************************/




/******************************************************************************
* Function Name: R_DEMO_AdpSetConfig
* Description :
* Arguments :
* Return Value :
******************************************************************************/
r_adp_status_t R_DEMO_AdpSetConfig(uint8_t chId)
{
    r_g3_set_config_req_t req = {0};
    r_g3_set_config_cnf_t *setcfgCfm;
    r_g3_config_extid_t  tmpExtId={0};

    R_BYTE_Uint64ToArr(g_demo_config.deviceEUI64, (uint8_t *)&tmpExtId);

    req.g3mode = R_G3_MODE_ADP;
    req.config.adp.bandPlan = g_demo_config.bandPlan;
    R_memcpy(req.config.adp.extendedAddress, (uint8_t *)&tmpExtId,8);
    R_memcpy(req.config.adp.psk, g_demo_config.pskKey,16);

    if(g_demo_config.routeType == R_G3_ROUTE_TYPE_JP_B)
    {
        req.config.adp.extIDFlg = R_TRUE;
        req.config.adp.pExtId = &tmpExtId;
        R_memcpy(req.config.adp.pExtId, &g_demo_config.extId, sizeof(r_g3_config_extid_t));
    }
    else
    {
        /**/
    }

    if(R_DEMO_G3SetConfig(chId, &req, &setcfgCfm) == R_RESULT_SUCCESS)
    {
        return (r_adp_status_t)setcfgCfm->status;
    }
    else
    {
        return (r_adp_status_t)R_DEMO_G3_STATUS_FAILED;
    }
}
/******************************************************************************
   End of function  R_DEMO_AdpSetConfig
******************************************************************************/

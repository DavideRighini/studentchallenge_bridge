/*******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
* Copyright (C) 2015 Renesas Electronics Corporation. All rights reserved.
*******************************************************************************/
/*******************************************************************************
* File Name   : r_demo_driver.c
*    @version
*        $Rev: 1663 $
*    @last editor
*        $Author: syama $
*    @date
*        $Date:: 2016-08-30 12:26:01 +0200#$
* Description : NetXDuo driver implementation based on the G3 PLC framework
******************************************************************************/

/******************************************************************************
Includes
******************************************************************************/
#include "nx_api.h"

#include "r_typedefs.h"
#include "r_config.h"
#include "r_stdio_api.h"
#include "r_byte_swap.h"
#include "r_c3sap_api.h"
#include "r_demo_app.h"
#include "r_demo_driver.h"
#include "r_demo_api.h"

#include "main_thread.h"

/******************************************************************************
* Defines
******************************************************************************/
#define R_G3_ADP_NUM_INTERFACES (1)       /* Number of G3 ADP interfaces (currently only supports one interface) */
#define R_G3_ADP_GROUP_TABLE_SIZE (16)    /* Size of group table in the G3 ADP layer */

/******************************************************************************
* Typedefs
******************************************************************************/
/* Define an application-specific data structure that holds internal
 data (such as the state information) of the G3 PLC device driver. */
typedef struct r_nx_g3_plc_driver_instance_t
{
    UINT nx_g3_plc_network_driver_in_use;
    UINT nx_g3_plc_network_driver_id;
    NX_INTERFACE *nx_g3_plc_driver_interface_ptr;
    NX_IP *nx_g3_plc_driver_ip_ptr;
} r_nx_g3_plc_driver_instance_t;

/*!
 \fn static r_result_t R_DEMO_G3_NetworkDriverOutput(NX_IP *ip_ptr, NX_PACKET *packet_ptr, UINT device_instance_id);
 \brief G3 PLC output driver for NetXDuo
 */
static r_result_t R_DEMO_G3_NetworkDriverOutput(NX_IP *ip_ptr, NX_PACKET *packet_ptr, UINT device_instance_id);

/* In G3 PLC there are up to two instances of the device driver, one for
   each channel. Therefore an array of driver instances is created to
   keep track of the device information of each driver. */
static r_nx_g3_plc_driver_instance_t nx_g3_plc_driver[R_G3_ADP_NUM_INTERFACES];

/* External structures. */
extern r_demo_entity_t  g_demo_entity;
extern NX_PACKET_POOL   pool_0;
extern r_demo_config_t  g_demo_config;

/******************************************************************************
   Functions
******************************************************************************/
void R_DEMO_G3_NetworkDriver(NX_IP_DRIVER *driver_req_ptr)
{
    uint8_t i;
    NX_IP *ip_ptr;
    NX_INTERFACE *interface_ptr;

    /* Setup the IP pointer from the driver request. */
    ip_ptr = driver_req_ptr->nx_ip_driver_ptr;

    /* Default to successful return. */
    driver_req_ptr->nx_ip_driver_status = NX_SUCCESS;

    /* Setup interface pointer. */
    interface_ptr = driver_req_ptr->nx_ip_driver_interface;

    /* Find out the driver interface if the driver command is not ATTACH. */
    if (driver_req_ptr->nx_ip_driver_command != NX_LINK_INTERFACE_ATTACH)
    {
        for (i = 0; i < R_G3_ADP_NUM_INTERFACES; i++)
        {
            if (nx_g3_plc_driver[i].nx_g3_plc_network_driver_in_use == 0)
            {
                continue;
            }

            if (nx_g3_plc_driver[i].nx_g3_plc_driver_ip_ptr != ip_ptr)
            {
                continue;
            }

            if (nx_g3_plc_driver[i].nx_g3_plc_driver_interface_ptr != driver_req_ptr->nx_ip_driver_interface)
            {
                continue;
            }

            break;
        }

        if (i == R_G3_ADP_NUM_INTERFACES)
        {
            driver_req_ptr->nx_ip_driver_status = NX_INVALID_INTERFACE;
            return;
        }
    }

    /* Process according to the driver request type in the IP control block. */
    switch (driver_req_ptr->nx_ip_driver_command)
    {
        case NX_LINK_INTERFACE_ATTACH:
            /* Find an available driver instance to attach the interface. */
            for (i = 0; i < R_G3_ADP_NUM_INTERFACES; i++)
            {
                if (nx_g3_plc_driver[i].nx_g3_plc_network_driver_in_use == 0)
                {
                    break;
                }
            }
            /* An available entry is found. */
            if (i < R_G3_ADP_NUM_INTERFACES)
            {
                /* Set the IN USE flag.*/
                nx_g3_plc_driver[i].nx_g3_plc_network_driver_in_use = 1;
                nx_g3_plc_driver[i].nx_g3_plc_network_driver_id = i;
                /* Record the interface attached to the IP instance. */
                nx_g3_plc_driver[i].nx_g3_plc_driver_interface_ptr = driver_req_ptr->nx_ip_driver_interface;
                /* Record the IP instance. */
                nx_g3_plc_driver[i].nx_g3_plc_driver_ip_ptr = ip_ptr;
            }
            else
            {
                driver_req_ptr->nx_ip_driver_status = NX_INVALID_INTERFACE;
            }
        break;
        case NX_LINK_INITIALIZE:
        {
            /* Device driver shall initialize the physical device here. */
#ifdef NX_DEBUG
            printf("NetX Duo G3 PLC Driver Initialization - %s\n", ip_ptr -> nx_ip_name);
            printf(" IP Address =%08X\n", ip_ptr -> nx_ip_address);
#endif
            /* Once the physical device is initialized, the driver needs to
               configure the NetX Duo Interface Control block, as outlined below. */

            /* The nx_interface_ip_mtu_size should be the MTU for the IP payload. */
            interface_ptr->nx_interface_ip_mtu_size = R_MAX_MTU_SIZE;

            /* This driver accepts IPv6 frames only. MAC address mapping is done
             * inside the ADP layer, so the interface's physical address is just
             * set to zero. */
            interface_ptr->nx_interface_physical_address_msw = 0;
            interface_ptr->nx_interface_physical_address_lsw = 0;

            /* Indicate to the IP software that IP to physical mapping is not
             * required for this IPv6 only driver. */
            interface_ptr->nx_interface_address_mapping_needed = NX_FALSE;
            break;
        }
        case NX_LINK_ENABLE:
        {
            /* Process driver link enable. An Ethernet driver shall enable the
             transmit and reception logic. Once the IP stack issues the
             LINK_ENABLE command, the stack may start transmitting IP packets. */

            /* For the G3 PLC driver implementation, just set the enabled flag. */
            interface_ptr->nx_interface_link_up = NX_TRUE;
#ifdef NX_DEBUG
            printf("NetX Duo G3 PLC Driver Link Enabled - %s\n", ip_ptr -> nx_ip_name);
#endif
            break;
        }

        case NX_LINK_DISABLE:
        {
            /* Process driver link disable. This command indicates the IP layer
             is not going to transmit any IP datagrams, nor does it expect any
             IP datagrams from the device. Therefore after processing this command,
             the device driver shall not send any incoming packets to the IP
             layer. Optionally the device driver may turn off the device. */

            /* For the G3 PLC driver implementation, just clear the enabled flag. */
            interface_ptr->nx_interface_link_up = NX_FALSE;
#ifdef NX_DEBUG
            printf("NetX Duo G3 PLC Driver Link Disabled - %s\n", ip_ptr -> nx_ip_name);
#endif
            break;
        }
        case NX_LINK_PACKET_SEND:
        {
            /* The IP stack sends down a data packet for transmission. */
#ifdef NX_DEBUG_PACKET
            printf("NetX Duo G3 PLC Driver Packet Send - %s\n", ip_ptr -> nx_ip_name);
#endif
            /* The network driver expects IPv6 frames, so directly forward the frame. */
            if (R_DEMO_G3_NetworkDriverOutput (ip_ptr, driver_req_ptr->nx_ip_driver_packet, i) != R_RESULT_SUCCESS)
            {
                driver_req_ptr->nx_ip_driver_status = NX_NO_RESPONSE;
            }
            break;
        }
        case NX_LINK_MULTICAST_JOIN:
        {
            uint8_t mcast_index;
            uint8_t newIndex                                = 0xFF;
            uint8_t curGroupTableEntry[R_ADP_MAX_IB_SIZE];
            uint8_t newGroupTableEntry[R_ADP_MAX_IB_SIZE]   = {0x01, 0xFF, 0xFF};

            /* The IP layer issues this command to join a multicast group. Note that
               multicast opertion is required for IPv6. */

            /* Derive group table address from IID. */
            R_BYTE_UInt16ToArr((uint16_t) ((driver_req_ptr->nx_ip_driver_physical_address_lsw & 0x1FFF) + 0x8000), &newGroupTableEntry[1]);

            /* In G3 PLC multicast functionality is provided by the ADP group table. */
            for (mcast_index = 0; mcast_index < R_G3_ADP_GROUP_TABLE_SIZE; mcast_index++)
            {
                if (R_DEMO_AdpmGetWrap(i, R_ADP_IB_GROUP_TABLE, mcast_index, curGroupTableEntry) == R_ADP_STATUS_SUCCESS)
                {
                    /* Check if inactive. */
                    if (curGroupTableEntry[0] == 0x00)
                    {
                        /* If not yet found, store free index. */
                        if (newIndex == 0xFF)
                        {
                            newIndex = mcast_index;
                        }
                    }
                    else
                    {
                        /* Check if same address already exists in table. */
                        if (memcmp(curGroupTableEntry, newGroupTableEntry, 3) == 0)
                        {
                            /* Store index and leave loop. */
                            newIndex = mcast_index;
                            break;
                        }
                    }
                }
            }

            /* Check if new entry can be written. */
            if (newIndex != 0xFF)
            {
                if (R_DEMO_AdpmSetWrap(i, R_ADP_IB_GROUP_TABLE, newIndex, newGroupTableEntry) == R_ADP_STATUS_SUCCESS)
                {
                    driver_req_ptr->nx_ip_driver_status = NX_SUCCESS;
                }
                else
                {
                    driver_req_ptr->nx_ip_driver_status = NX_NO_MORE_ENTRIES;
                }
            }
            else
            {
                driver_req_ptr -> nx_ip_driver_status = NX_NO_MORE_ENTRIES;
            }

            break;
        }
        case NX_LINK_MULTICAST_LEAVE:
        {
            uint8_t mcast_index;
            uint8_t curGroupTableEntry[R_ADP_MAX_IB_SIZE];
            uint8_t newGroupTableEntry[R_ADP_MAX_IB_SIZE] = {0x01, 0xFF, 0xFF};;

            /* The IP layer issues this command to remove a multicast MAC address from the
             receiving list. */

            /* Derive group table address from IID. */
            R_BYTE_UInt16ToArr((uint16_t)((driver_req_ptr->nx_ip_driver_physical_address_lsw & 0x1FFF) + 0x8000), &newGroupTableEntry[1]);

            /* In G3 PLC multicast functionality is provided by the ADP group table. */
            for (mcast_index = 0; mcast_index < R_G3_ADP_GROUP_TABLE_SIZE; mcast_index++)
            {
                if (R_DEMO_AdpmGetWrap(i, R_ADP_IB_GROUP_TABLE, mcast_index, curGroupTableEntry) == R_ADP_STATUS_SUCCESS)
                {
                    /* Check if same address already exists in table. */
                    if (memcmp(curGroupTableEntry, newGroupTableEntry, 3) == 0)
                    {
                        /* Set new entry to all zero to deactivate it. */
                        memset(newGroupTableEntry, 0, R_ADP_MAX_IB_SIZE);

                        if (R_DEMO_AdpmSetWrap(i, R_ADP_IB_GROUP_TABLE, mcast_index, newGroupTableEntry) == R_ADP_STATUS_SUCCESS)
                        {
                            driver_req_ptr->nx_ip_driver_status = NX_SUCCESS;
                        }
                        else
                        {
                            driver_req_ptr->nx_ip_driver_status = NX_NO_MORE_ENTRIES;
                        }

                        break;
                    }
                }
            }

            if(mcast_index == R_G3_ADP_GROUP_TABLE_SIZE)
            {
                driver_req_ptr -> nx_ip_driver_status = NX_ENTRY_NOT_FOUND;
            }

            break;
        }
        case NX_LINK_GET_STATUS:
        {
            /* Return the link status in the supplied return pointer. */
            *(driver_req_ptr->nx_ip_driver_return_ptr) = ip_ptr->nx_ip_interface[0].nx_interface_link_up;
            break;
        }
        case NX_LINK_DEFERRED_PROCESSING:
        {
            /* Driver defined deferred processing. This is typically used to defer interrupt
             processing to the thread level.
             A typical use case of this command is:
             On receiving an Ethernet frame, the RX ISR does not process the received frame,
             but instead records such an event in its internal data structure, and issues
             a notification to the IP stack (the driver sends the notification to the IP
             helping thread by calling "_nx_ip_driver_deferred_processing()". When the IP stack
             gets a notification of a pending driver deferred process, it calls the
             driver with the NX_LINK_DEFERRED_PROCESSING command. The driver shall complete
             the pending receive process.
             */
            /* The G3 IPv6 driver doesn't require a deferred process so it breaks out of
             the switch case. */
            break;
        }
        default:
        {
            /* Not supported by this IPv6 only driver:
               NX_LINK_ARP_SEND,
               NX_LINK_ARP_RESPONSE_SEND,
               NX_LINK_RARP_SEND,
               NX_LINK_PACKET_BROADCAST. */

            /* Invalid driver request. */
            /* Return the unhandled command status. */
            driver_req_ptr->nx_ip_driver_status = NX_UNHANDLED_COMMAND;
#ifdef NX_DEBUG
            printf("NetX Duo G3 PLC Driver Received invalid request - %s\n", ip_ptr -> nx_ip_name);
#endif
        }
    }
}

static r_result_t R_DEMO_G3_NetworkDriverOutput(NX_IP *ip_ptr, NX_PACKET *packet_ptr, UINT device_instance_id)
{
    UINT                    old_threshold;
    r_adp_adpd_data_req_t   request;
    r_adp_adpd_data_cnf_t*  confirm;
    r_result_t              result;

    UNUSED (ip_ptr);

    /* Parameter check. */
    if (device_instance_id > R_G3_ADP_NUM_INTERFACES)
    {
        return R_RESULT_FAILED;
    }

    /* To follow the certification test specification ICMP ID value has to be set to 0x0102. As a work around this is
       currently done in the demo driver implementation. */
    if(g_demo_config.appMode == R_DEMO_MODE_CERT)
    {
        /* Check if this is an ICMP Echo frame. */
        if ((packet_ptr->nx_packet_ip_header[6] == 58) && (packet_ptr->nx_packet_ip_header[40] == 128))
        {
            /* Set ICMP ID. */
            packet_ptr->nx_packet_ip_header[44] = 0x01;
            packet_ptr->nx_packet_ip_header[45] = 0x02;
        }
    }

    /* Set request structure. */
    request.nsduLength          = (uint16_t) packet_ptr->nx_packet_length;
    request.pNsdu               = packet_ptr->nx_packet_ip_header;
    request.nsduHandle          = g_demo_entity.nsduHandle++;
    request.discoverRoute       = g_demo_config.discoverRoute;
    request.qualityOfService    = g_demo_config.qualityOfService;

    /* Disable preemption. */
    tx_thread_preemption_change (tx_thread_identify (), 0, &old_threshold);

    /* Send frame to adaptation layer. */
    result = R_DEMO_AdpdData(R_DEMO_G3_USE_PRIMARY_CH,
                             &request,
                             &confirm);

    /* If the function returns successfully, also check the status
       value in the confirm structure. */
    if (R_RESULT_SUCCESS == result)
    {
        /* If the transmission was not successful, set result value accordingly. */
        if (R_ADP_STATUS_SUCCESS != confirm->status)
        {
            result = R_RESULT_FAILED;
        }
    }

    /* Now that the frame has been sent, release the packet. */
    nx_packet_transmit_release(packet_ptr);

    /* Restore preemption. */
    tx_thread_preemption_change (tx_thread_identify (), old_threshold, &old_threshold);

    return result;
}

void R_DEMO_G3_NetworkDriverReceive(NX_IP *ip_ptr, const r_adp_adpd_data_ind_t* indication, UINT device_instance_id)
{
    NX_PACKET *packet_ptr;

    /* Parameter check. */
    if (device_instance_id > R_G3_ADP_NUM_INTERFACES)
    {
        return;
    }

    /* Allocate NetX packet. */
    if (nx_packet_allocate(&pool_0,
                           &packet_ptr,
                           NX_RECEIVE_PACKET,
                           TX_NO_WAIT) != NX_SUCCESS)
    {
        return;
    }

    /* Setup interface pointer. */
    packet_ptr -> nx_packet_ip_interface = nx_g3_plc_driver[device_instance_id].nx_g3_plc_driver_interface_ptr;

    /* Append receive packet. */
    if (nx_packet_data_append(packet_ptr,
                              indication->pNsdu,
                              indication->nsduLength,
                              &pool_0,
                              TX_NO_WAIT) != NX_SUCCESS)
    {
        return;
    }

   /* Forward to the IP receive function. */
#ifdef NX_DEBUG_PACKET
        printf("NetX Duo G3 PLC Driver IP Packet Receive - %s\n", ip_ptr -> nx_ip_name);
#endif
#ifdef NX_DIRECT_ISR_CALL
        _nx_ip_packet_receive(ip_ptr, packet_ptr);
#else
        _nx_ip_packet_deferred_receive (ip_ptr, packet_ptr);
#endif
   /* Release received packet. */
   nx_packet_release(packet_ptr);
}

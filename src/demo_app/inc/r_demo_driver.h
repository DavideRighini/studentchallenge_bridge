/*******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
* Copyright (C) 2015 Renesas Electronics Corporation. All rights reserved.
*******************************************************************************/
/*******************************************************************************
* File Name   : r_demo_driver.h
*    @version
*        $Rev: 1127 $
*    @last editor
*        $Author: syama $
*    @date  
*        $Date:: 2016-02-10 11:20:00 +0100#$
* Description : Provides API functions for the G3 PLC NetXDuo sample driver
******************************************************************************/

#ifndef R_DEMO_DRIVER_H_
#define R_DEMO_DRIVER_H_

/******************************************************************************
Macro definitions
******************************************************************************/
/******************************************************************************
Typedef definitions
******************************************************************************/
/******************************************************************************
Functions prottype
******************************************************************************/

/***********************************************************************
* Function Name     : R_DEMO_G3_NetworkDriver
* Description       : Main G3 PLC NetXDuo driver control function
* Argument          : driver_req_ptr : Pointer to NX IP driver structure
* Return Value      : NONE
***********************************************************************/
void R_DEMO_G3_NetworkDriver(NX_IP_DRIVER *driver_req_ptr);

/***********************************************************************
* Function Name     : R_DEMO_G3_NetworkDriverReceive
* Description       : Receive handling function for the G3 PLC NetXDuo driver
* Argument          : ip_ptr : Pointer to NX IP driver structure
*                   : indication: Pointer to the received frame
*                   : device_instance_id: Device instance ID
* Return Value      : NONE
***********************************************************************/
void R_DEMO_G3_NetworkDriverReceive(NX_IP *ip_ptr, const r_adp_adpd_data_ind_t* indication, UINT device_instance_id);

#endif /* R_DEMO_DRIVER_H_ */


/******************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only
 * intended for use with Renesas products. No other uses are authorized. This
 * software is owned by Renesas Electronics Corporation and is protected under
 * all applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
 * THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
 * LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
 * AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
 * TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
 * ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
 * FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
 * ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
 * BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this software
 * and to discontinue the availability of this software. By using this software,
 * you agree to the additional terms and conditions found by accessing the
 * following link:
 * http://www.renesas.com/disclaimer
 *
 * Copyright (C) 2015 Renesas Electronics Corporation. All rights reserved.
 ******************************************************************************/

/*******************************************************************************
 * File Name : r_printf_usb_thread_entry.c
 * Description : USB Printf entry
 ******************************************************************************/

/******************************************************************************
 * Includes
 ******************************************************************************/
#include "r_printf_usb_thread.h"

#define TX_BUFFER_LEN (64u)

static uint8_t tx_msg[TX_BUFFER_LEN];

/*!
 \fn void r_printf_usb_thread_entry(void)
 \brief Printf USB thread entry function.
 */
void r_printf_usb_thread_entry(void)
{
    size_t length = 0;

    /* Enter infinite loop and check queue for new content. */
    while (1)
    {
        /* Wait for something to be received in the queue. */
        tx_queue_receive (&g_r_printf_queue, tx_msg, TX_WAIT_FOREVER);

        length = 0;

        /* Send the message via USB CDC */
        while ((length < TX_BUFFER_LEN) && (tx_msg[length] != 0))
        {
            ++length; /* if there is no NULL in buffer, length will increase to TX_BUFFER_LEN, otherwise it is string length without NULL. */
        }

        /* Call write function. */
        g_sf_comms.p_api->write (g_sf_comms.p_ctrl, tx_msg, length, TX_WAIT_FOREVER);
    }
}

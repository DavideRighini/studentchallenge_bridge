#include "lora_main.h"


/* Lora Main entry function */
#include "main_thread.h"
#include "message_size.h"
#include "r_printf_usb_thread.h"
#include "r_demo_api.h"
#include "r_typedefs.h"
#include "r_config.h"
#include "r_bsp_api.h"
#include "r_stdio_api.h"

#include "r_demo_main.h"

#include "r_printf_usb_thread.h"
#include "r_demo_common.h"
#include "r_stdio_api.h"

#include <stdlib.h>
#include <string.h>
#include "r_typedefs.h"
#include "r_config.h"
#include "r_byte_swap.h"
#include "r_c3sap_api.h"
#include "r_g3_sap.h"
#include "r_adp_sap.h"

#include "r_demo_app.h"
#include "r_demo_common.h"
#include "r_demo_api.h"
#include "r_stdio_api.h"
extern TX_THREAD lora_main;
extern TX_THREAD main_thread;


static volatile bool transmitComplete = false;
static volatile uint16_t UART_charReceived = 0;
static volatile uint8_t UART_dest[256] = {0};

static volatile bool UART_Receive = false;
static volatile uint16_t UART_Size = 0;

void lora_main_entry(void);

/* Main Thread entry function */
void lora_main_entry(void)
{
    /* TODO: add your own code here */
    UINT status;
    ssp_err_t open_result, write_result;
    volatile uint8_t USB_charReceived = 0;
    uint8_t USB_dest[256] = {0};

    /*Open UART*/
    open_result = g_uart1.p_api->open(g_uart1.p_ctrl, g_uart1.p_cfg);

    if (SSP_SUCCESS != open_result)

    {
        while(1); /*Could not open UART*/
    }

    /*Infinite loop*/
    while (1)
    {

        if(UART_Receive){
        	uint8_t        q_output [16] = {0};
        	for (int i = 0; i <= 16; i++)
        	{
        		q_output[i] = 	UART_dest[i];
        	}
            status = tx_queue_send(&g_bridge_input_plc_to_lora_queue, q_output, TX_NO_WAIT);
			
            if(status == TX_SUCCESS)
            {
				q_output[0] = '\0';
				tx_thread_suspend(&lora_main);

            }
            UART_Receive = false;
        }

        else
        {
            tx_thread_sleep(1); /* We didn't receive anything */
        }


    }
}


/* UART Callback function
 * Set Transmit Done flag when UART transmission is complete.
 * Manage received UART bytes.
*/
void user_uart_callback(uart_callback_args_t *p_args)
{

    static bool firstChar = true;
    if (p_args->event == UART_EVENT_TX_COMPLETE)/*Transmit complete event*/
    {
        // Set Transmit Complete Flag
        transmitComplete = true;
    }
    if(p_args->event == UART_EVENT_RX_CHAR)/*Received char event*/
    {
        if(firstChar)
        {
            /* First char is UART message size */
            UART_Size = p_args->data;
            firstChar = false;
        }
        else
        {
            if(UART_Size != 0)
            {
                /* Next bytes are the payload*/
                UART_dest[UART_charReceived] = p_args->data;
                UART_charReceived++;
                if(UART_Size == UART_charReceived)
                {
                    /*If message if complete*/
                    firstChar = true;
                    UART_charReceived = 0;
                    UART_Receive = true;/*Set UART receive flag to true*/
                }
            }
            else
            {
                firstChar = true;
            }
        }
    }

}


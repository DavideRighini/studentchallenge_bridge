/******************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only
 * intended for use with Renesas products. No other uses are authorized. This
 * software is owned by Renesas Electronics Corporation and is protected under
 * all applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
 * THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
 * LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
 * AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
 * TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
 * ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
 * FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
 * ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
 * BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this software
 * and to discontinue the availability of this software. By using this software,
 * you agree to the additional terms and conditions found by accessing the
 * following link:
 * http://www.renesas.com/disclaimer
 *
 * Copyright (C) 2015 Renesas Electronics Corporation. All rights reserved.
 ******************************************************************************/

/*******************************************************************************
 * File Name :main_thread_entry.c
 * Description : Main thread entry
 ******************************************************************************/

/******************************************************************************
 * Includes
 ******************************************************************************/
#include "main_thread.h"
#include "r_typedefs.h"
#include "r_config.h"
#include "r_bsp_api.h"
#include "r_stdio_api.h"

#include "r_demo_main.h"

#include "r_printf_usb_thread.h"

/*!
 \fn void main_thread_entry(void)
 \brief Main thread entry function.
 */
void main_thread_entry(void)
{
    ssp_err_t result;

    /* Set board type */
    R_BSP_SetBoardType (R_BOARD_G_CPX3);

    /* Activate LEDs */
    R_BSP_InitLeds ();

    /* Stdio init */
    result = R_STDIO_Init ();
    if (SSP_SUCCESS != result)
    {
        /* Indicate error. */
        R_BSP_LedOn (R_BSP_LED_4);
        while (1)
        {
            /* Toggle LED and stay here. */
        }
    }

    /* Start demo application menu. */
    R_DEMO_Main ();
}
